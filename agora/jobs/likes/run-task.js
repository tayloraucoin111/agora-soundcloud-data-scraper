const { captureLikes } = require("./steps/likes-page");
const { getLikesCount } = require("./steps/artist-page");
const { searchArtist } = require("./steps/search-artist");

const {
  delay,
  elementClick,
  selectorExists,
  timeDiff,
} = require("../../utils");

const DOMAIN_URL = "https://soundcloud.com";
const BASE_URL = `${DOMAIN_URL}/search?q=`;

const runLikesTask = async (page, name) => {
  const timeStart = new Date().getTime();
  const URL = `${BASE_URL}/${name}`;

  await page.goto(DOMAIN_URL);

  const cookies = [
    {
      name: "OptanonConsent",
      value:
        "isIABGlobal=false&datestamp=Sun+May+23+2021+16%3A54%3A15+GMT-0700+(Pacific+Daylight+Time)&version=6.16.0&hosts=&consentId=77d27aaf-c5a7-43f0-a41e-f10ab33d4a5b&interactionCount=1&landingPath=NotLandingPage&groups=C0001%3A1%2CC0003%3A0%2CC0002%3A0%2CC0004%3A0%2CC0007%3A0&iType=6",
    },
    {
      name: "OptanonAlertBoxClosed",
      value: "2021-05-23T23:54:15.311Z",
    },
  ];

  await page.setCookie(...cookies);

  console.log(`Navigating to ${URL}...`);
  await page.goto(URL);

  // Wait for the required DOM to be rendered
  await page.waitForSelector("#app");
  delay(500);

  /* Find artist from list and click to enter page */
  const artistFound = await searchArtist(page, name);
  console.log("artistFound", artistFound);

  if (!artistFound) {
    return {
      data: `no artist found for ${name}`,
    };
  }

  console.log(`STARTING LIKES COUNT ${name}`);
  const likes = await getLikesCount(page);
  const likesTotalCount = parseFloat(likes);
  console.log("likesTotalCount", likesTotalCount);
  if (!likesTotalCount) {
    return {
      data: `no likes found for ${name}`,
    };
  }

  const currentUrl = await page.url();
  await page.goto(`${currentUrl}/likes`);
  // await page.click(".likesModule .sidebarHeader__actualTitle");

  console.log(`STARTING LIKES DATA FOR ${name}`);
  // const likesData = likesTotalCount && (await captureLikes(page));
  const likesData = await captureLikes(page, name);
  console.log(`FINISHED LIKES DATA FOR ${name}`, likesData.length);

  const timeEnd = new Date().getTime();

  const timeTaken = timeDiff(timeStart, timeEnd);

  console.log(
    `TIME TO COMPLETE ${likesTotalCount} for ${name} was ${timeTaken}`
  );
  console.log(
    `RATIO OF PROCESSING TIME PER LIKE WAS ${
      (timeEnd - timeStart) / 1000 / likesTotalCount
    }`
  );

  const unixTimeTaken = timeEnd - timeStart;

  return {
    data: likesData || [],
    likesTotalCount: likesTotalCount || likesData.length,
    matchRate: likesTotalCount && likesTotalCount - likesData.length,
    secondsPerLike: (unixTimeTaken / 1000) / likesData.length,
    timeTaken: timeTaken,
    unixTimeTaken,
  };
};

module.exports = runLikesTask;
