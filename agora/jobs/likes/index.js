const fs = require("fs");
const puppeteer = require("puppeteer");

const { delay } = require("../../utils");
// const BASS_JSON = require("../../constants/bass-test-long.json");
const FRACTAL_JSON = require("../../constants/fractal-full.json");
// const TOYMAKER_BASS_JSON = require("../../constants/toymaker-short-list.json");
const runLikesTask = require("./run-task");
const formatData = require("./utils/format-data");

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

/* each command is an User, but keep type as directive */
async function getUserLikes() {
  const dataMatrix = {};

  const maxBrowserInstances = 8;
  let activeBrowsers = 0;

  const mapData = async () => {
    return Promise.all(
      FRACTAL_JSON.map(async (n) => {
        while (activeBrowsers == maxBrowserInstances) {
          await sleep(1000);
        }

        activeBrowsers += 1;

        const browser = await puppeteer.launch({
          headless: false,
          args: ["--disable-setuid-sandbox", "--disable-dev-shm-usage"],
          ignoreHTTPSErrors: true,
        });

        let page = await browser.newPage();

        const likes = await runLikesTask(page, n.name);
        const temp = {
          dataLength: likes.data.length,
          likesTotalCount: likes.likesTotalCount,
          matchRate: likes.matchRate,
          secondsPerLike: likes.secondsPerLike,
          timeTaken: likes.timeTaken,
          unixTimeTaken: likes.unixTimeTaken,
        };
        console.log(`LIKES COMPLETE ${n.name}: `, temp);
        dataMatrix[n.name] = likes;
        delay(100);

        activeBrowsers -= 1;
        await browser.close();
      })
    );
  };
  await mapData();

  /* format data via util */
  const dataFormatted = formatData(dataMatrix);

  /* write JSON file! */
  fs.writeFile(
    "data.json",
    JSON.stringify({
      artistData: dataMatrix,
      trackData: dataFormatted,
    }),
    "utf8",
    function (err) {
      if (err) {
        return console.log(err);
      }
      console.log(
        "The data has been scraped and saved successfully! View it at './data.json'"
      );
    }
  );

  console.log("dataMatrix", dataMatrix);
}

module.exports = getUserLikes;
