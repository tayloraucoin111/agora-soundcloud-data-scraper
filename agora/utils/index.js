const clearOneOffs = (matrix) => {
  const cleared = {};
  Object.keys(matrix).forEach((key) => {
    if (matrix[key].quantity > 1) {
      cleared[key] = matrix[key];
    }
  });
  return cleared;
};

const delay = (time) => {
  return new Promise(function (resolve) {
    setTimeout(resolve, time);
  });
};

const elementClick = async (page, selector, timeout = 30000) => {
  await page.waitForSelector(selector, { visible: true, timeout });

  let error;
  while (timeout > 0) {
    try {
      await page.click(selector);
      return;
    } catch (e) {
      await page.waitFor(100);
      timeout -= 100;
      error = e;
    }
  }
  throw error;
};

const getTextFromElement = async (page, element, waitForSelector) => {
  if (waitForSelector) await page.waitForSelector(waitForSelector);
  return await page.evaluate((el) => el.textContent, element);
};

const getTextFromSelector = async (page, selector) => {
  await page.waitForSelector(selector);
  return await getTextFromElement(page, await page.$(selector));
};

const objectToArray = (object) => Object.keys(object).map((key) => object[key]);

const reduceCharacters = (string) => {
  return string.toLowerCase().replace(/[^0-9a-z-]/g, "");
};

const selectorExists = async (page, selector) => {
  return await page.$$(selector);
};

function sorter(items, condition, field) {
  let sorted;

  switch (condition) {
    case "number_highest":
      sorted = sortByNumberHighest(items, field);
      break;
    case "number_lowest":
      sorted = sortByNumberLowest(items, field);
      break;
    default:
      if (items) sorted = items.sort(sortArray);
      break;
  }

  function sortArray(a, b) {
    if (a < b) return -1;
    if (a > b) return 1;
    return 0;
  }

  function sortByNumberHighest(list, key) {
    return list.sort((a, b) => b[key] - a[key]);
  }

  function sortByNumberLowest(list, key) {
    return list.sort((a, b) => a[key] - b[key]);
  }

  return sorted;
}

function timeDiff(tstart, tend) {
  var diff = Math.floor((tend - tstart) / 1000),
    units = [
      { d: 60, l: "seconds" },
      { d: 60, l: "minutes" },
      { d: 24, l: "hours" },
    ];

  var s = "";
  for (var i = 0; i < units.length; ++i) {
    s = (diff % units[i].d) + " " + units[i].l + " " + s;
    diff = Math.floor(diff / units[i].d);
  }
  return s;
}

const typeInInput = async (page, selector, text) => {
  console.log("focus", selector);
  await page.waitForSelector(selector);
  await page.type(selector, text);
};

module.exports = {
  clearOneOffs,
  delay,
  elementClick,
  getTextFromElement,
  getTextFromSelector,
  objectToArray,
  reduceCharacters,
  selectorExists,
  sorter,
  timeDiff,
  typeInInput,
};
