const { Cluster } = require("puppeteer-cluster");

const { sorter } = require("./utils");
const likes = require("./tasks/likes/likes");
const FRACTAL_JSON = require("./constants/fractal-test.json");

const JSON_DATA = require("../data.json");

const artists = sorter([
  ...new Set(
    FRACTAL_JSON.map((item) => {
      return item.name;
    })
  ),
]);

/* manual value to run what task operation */
const task = "likes";

(async () => {
  /* initializes puppeteer cluster */
  const cluster = await Cluster.launch({
    concurrency: Cluster.CONCURRENCY_CONTEXT,
    // maxConcurrency: 10,
    maxConcurrency: 1,
  });

  // const dataParsed = JSON.parse(JSON_DATA);

  // console.log("keys", Object.keys(JSON_DATA));
  // // console.log("keys", Object.keys(JSON_DATA.trackData));
  // // console.log("keys", Object.keys(JSON_DATA.artistData));
  // console.log("artistData", JSON_DATA.artistData);
  // console.log("trackData", JSON_DATA.trackData);

  if (task === "likes") {
    await likes(cluster, artists);
  }
})();
