const fs = require("fs");

const formatData = require("./utils/format-data");
const runLikesTask = require("./run-task");

const likesTask = async (cluster, artists) => {
  /* where scraped data will be stored */
  const dataMatrix = {};

  /* add all artist data into the cluster queue */
  artists.forEach((artistName) => {
    cluster.queue(artistName);
  });

  /* run cluster task operations from the queue */
  await cluster.task(async ({ page, data: name }) => {
    console.log("starting: ", name);
    const likes = await runLikesTask(page, name);
    console.log(`LIKES COMPLETE ${name}: `, likes);
    dataMatrix[name] = likes;
  });

  /* wait for queued operations to complete */
  console.log("idle start");
  await cluster.idle();

  /* complete cluster operation */
  console.log("about to close - DATA SCRAPE COMPLETE");
  await cluster.close();

  /* format data via util */
  const dataFormatted = formatData(dataMatrix);

  /* write JSON file! */
  fs.writeFile(
    "data.json",
    JSON.stringify({
      artistData: dataMatrix,
      trackData: dataFormatted,
    }),
    "utf8",
    function (err) {
      if (err) {
        return console.log(err);
      }
      console.log(
        "The data has been scraped and saved successfully! View it at './data.json'"
      );
    }
  );
};

module.exports = likesTask;
