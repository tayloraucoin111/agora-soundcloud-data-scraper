const { captureLikes } = require("./steps/likes-page");
const { getLikesCount } = require("./steps/artist-page");
const { searchArtist } = require("./steps/search-artist");

const { timeDiff } = require("../../utils");

const baseUrl = "https://soundcloud.com/search?q=";

const runLikesTask = async (page, name) => {
  const timeStart = new Date().getTime();
  const url = `${baseUrl}/${name}`;

  console.log(`Navigating to ${url}...`);
  await page.goto(url);

  // Wait for the required DOM to be rendered
  await page.waitForSelector("#app");

  /* Find artist from list and click to enter page */
  const artistFound = await searchArtist(page, name);
  console.log("artistFound", artistFound);

  if (!artistFound) {
    return {
      data: `no artist found for ${name}`,
    };
  }

  // console.log(`STARTING LIKES COUNT ${name}`);
  // const likesTotalCount = await getLikesCount(page);
  // console.log("likesTotalCount", likesTotalCount);
  // if (!likesTotalCount) {
  //   return {
  //     data: `no likes found for ${name}`,
  //   };
  // }

  const currentUrl = await page.url();
  await page.goto(`${currentUrl}/likes`);
  // await page.click(".likesModule .sidebarHeader__actualTitle");

  console.log(`STARTING LIKES DATA FOR ${name}`);
  // const likesData = likesTotalCount && (await captureLikes(page));
  const likesData = await captureLikes(page, name);
  console.log(`FINISHED LIKES DATA FOR ${name}`, likesData);
  console.log(
    `FINISHED LIKES DATA FOR ${name}, final count: ${likesData.length}`
  );

  const timeEnd = new Date().getTime();

  const timeTaken = timeDiff(timeStart, timeEnd);

  // console.log(
  //   `TIME TO COMPLETE ${likesTotalCount} for ${name} was ${timeTaken}`
  // );
  // console.log(
  //   `RATIO OF PROCESSING TIME PER LIKE WAS ${
  //     (timeEnd - timeStart) / 1000 / likesTotalCount
  //   }`
  // );

  return {
    data: likesData || [],
    // likesTotalCount: likesTotalCount
    //   ? parseInt(likesTotalCount)
    //   : likesData.length,
    // matchRate: likesTotalCount && likesTotalCount - likesData.length,
    unixTimeTaken: timeEnd - timeStart,
  };
};

module.exports = runLikesTask;
