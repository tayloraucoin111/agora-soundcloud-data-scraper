const { reduceCharacters } = require("../../../utils");

/*
  TODO:
    - check name value for match
*/

const searchArtist = async (page, name) => {
  let pageToClick;
  const reducedName = reduceCharacters(name);

  await page.waitForSelector(".sc-link-dark");

  /* Find match via href match */
  const titles = await page.evaluate(() =>
    Array.from(
      document.querySelectorAll(".userItem__title .sc-link-dark"),
      (element) => element.textContent
    )
  );
  titles.find((v, index) => {
    if (reduceCharacters(v) === reducedName) {
      pageToClick = index + 1;
      return true;
    }
  });

  /* Find match via href match */
  if (!pageToClick) {
    const hrefs = await page.evaluate(() =>
      Array.from(document.querySelectorAll(".userItem__title a[href]"), (a) =>
        a.getAttribute("href")
      )
    );
    hrefs.find((h, index) => {
      if (reduceCharacters(h) === reducedName) {
        pageToClick = index + 1;
        return true;
      }
    });
  }

  /* Click on artist page */
  if (pageToClick) {
    await page.click(
      `.lazyLoadingList__list .sc-link-dark:nth-child(${pageToClick})`
    );
  }
  return pageToClick;
};

module.exports = {
  searchArtist,
};
