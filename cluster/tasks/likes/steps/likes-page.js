const { delay } = require("../../../utils");

const captureLikes = async (page, name) => {
  await page.waitForSelector(".soundTitle__title");

  console.log("starting likes scrape...");

  const grabData = async () => {
    console.log("in grabData");
    return await page.evaluate(() => {
      window.scrollTo(0, 0);
      let products = [];
      let productElements = document.querySelectorAll(".sound__content");

      productElements.forEach((productElement) => {
        let productJson = {};
        try {
          productJson.artistName = productElement.querySelector(
            ".sound__content .soundTitle__username"
          ).innerText;
          productJson.artistUrl = productElement.querySelector(
            ".sound__content .soundTitle__username"
          ).href;
          productJson.trackLikes =
            productElement.querySelector(".sc-button-like").innerText;
          productJson.trackReposts =
            productElement.querySelector(".sc-button-repost").innerText;
          productJson.title =
            productElement.querySelector(".soundTitle__title").innerText;
          productJson.url =
            productElement.querySelector(".soundTitle__title").href;
        } catch (e) {
          console.log(e);
        }
        products.push(productJson);
      });
      return products;
    });
  };

  const delayRate = 4000;

  let viewportIncr = 0;

  async function scrollToBottom() {
    console.log("in scrollToBottom", viewportIncr);
    // Get the height of the rendered page
    const bodyHandle = await page.$("body");
    const { height } = await bodyHandle.boundingBox();
    await bodyHandle.dispose();

    // Scroll one viewport at a time, pausing to let content load
    const viewportHeight = page.viewport().height;

    console.log("viewportIncr", viewportIncr);
    console.log("viewportHeight", viewportHeight);
    console.log("viewportIncr + viewportHeight", viewportIncr + viewportHeight);
    console.log("height", height);
    console.log("WHILE", viewportIncr + viewportHeight < height);

    while (viewportIncr + viewportHeight < height) {
      await page.evaluate((_viewportHeight) => {
        window.scrollBy(0, _viewportHeight);
      }, viewportHeight);
      await delay(delayRate);
      viewportIncr = viewportIncr + viewportHeight;
      console.log("CALL scrollToBottom AGAIN");
      await scrollToBottom();
    }
    console.log("TO GET DATA", viewportIncr + viewportHeight === height);
    // if (viewportIncr + viewportHeight === height) {
    //   console.log(`${name} EXITING AT ${viewportIncr}`);
    //   return await grabData();
    // }
    return await grabData();
    console.log("EXIT scrollToBottom");
  }
  console.log("await scrollToBottom");
  return await scrollToBottom();
};

module.exports = {
  captureLikes,
};
