const { clearOneOffs, objectToArray, sorter } = require("../../../utils");

const formatData = (dataMatrix) => {
  /* format data for file save */
  const compileData = () => {
    const inputMatrix = {};

    Object.keys(dataMatrix).forEach((key) => {
      const artistData = dataMatrix[key];
      if (typeof artistData.data == "string") return;
      artistData.data.forEach((item) => {
        if (inputMatrix[item.url]) {
          inputMatrix[item.url].quantity = inputMatrix[item.url].quantity + 1;
          inputMatrix[item.url].likedBy.push(key);
        } else {
          inputMatrix[item.url] = {
            data: item,
            likedBy: [key],
            quantity: 1,
          };
        }
      });
    });

    return inputMatrix;
  };
  //   const compiledData = clearOneOffs(compileData());
  const compiledData = compileData();
  console.log("compiledData", compiledData);

  /* console.log the sorted results */
  const sorted = sorter(
    objectToArray(compiledData),
    "number_highest",
    "quantity"
  );
  console.log("sorted", sorted);

  return compiledData;
};

module.exports = formatData;
