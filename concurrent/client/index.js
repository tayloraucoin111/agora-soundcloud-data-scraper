let axios = require("axios");
let ldb = require("./lowdbHelper.js").LowDbHelper;
let ldbHelper = new ldb();
let allBooks = ldbHelper.getData();

const FRACTAL_JSON = require("../../cluster/constants/fractal-test.json");

// let server = "http://your_load_balancer_external_ip_address";
let server = "http://localhost:5000";
let podsWorkDone = [];
let booksDetails = [];
let errors = [];

/* Initialize command reception from start sequence - begins scrape operation*/
function go() {
  let execute = process.argv[2] ? process.argv[2] : 0;
  execute = parseInt(execute);
  switch (execute) {
    case 0:
      getUserLikes();
      break;
    case 1:
      break;
  }
}

const USER_COMMANDS = FRACTAL_JSON.map((n) => {
  return {
    user: {
      ...n,
    },
    type: "getUserLikes",
  };
});

/* each command is an User, but keep type as directive */
function getUserLikes() {
  console.log("getting user-likes");
  let data = {
    commands: USER_COMMANDS,
    task: "user-likes",
  };
  let begin = Date.now();
  axios.post(`${server}/api/user-likes`, data).then((result) => {
    let end = Date.now();
    let timeSpent = (end - begin) / 1000 + "secs";
    console.log(
      `took ${timeSpent} to retrieve ${result.data.userLikes.length} user-like-sets`
    );
    ldbHelper.saveData(result.data.userLikes);
  });
}

go();
