const express = require("express");
const bodyParser = require("body-parser");
const os = require("os");

let pMng = require("./puppeteer-manager");

const PORT = 5000;
const app = express();
let timeout = 1500000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

let browsers = 0;
let maxNumberOfBrowsers = 5;

app.get("/", (req, res) => {
  console.log(os.hostname());
  let response = {
    msg: "hello world",
    hostname: os.hostname().toString(),
  };
  res.send(response);
});

app.post("/api/user-likes", async (req, res) => {
  req.setTimeout(timeout);
  try {
    let data = req.body;
    console.log("TASK OPERATION:", req.body.task);
    console.log("COMMAND LENGTH:", req.body.commands.length);
    while (browsers == maxNumberOfBrowsers) {
      await sleep(1000);
    }
    await getUserLikesHandler(data).then((result) => {
      let response = {
        msg: "retrieved user-likes ",
        hostname: os.hostname(),
        userLikes: result,
      };
      console.log("done");
      res.send(response);
    });
  } catch (error) {
    res.send({ error: error.toString() });
  }
});

async function getUserLikesHandler(arg) {
  console.log("getUserLikesHandler");
  console.log("pMng");
  console.log("pMng", JSON.stringify(pMng));
  let puppeteerMng = new pMng.PuppeteerManager(arg);
  console.log("puppeteerMng");
  browsers += 1;
  console.log("browsers", browsers);
  try {
    console.log("starting getUserLikes");
    let userLikes = await puppeteerMng.getUserLikes().then((result) => {
      console.log("user likes returned");
      return result;
    });
    browsers -= 1;
    return userLikes;
  } catch (error) {
    browsers -= 1;
    console.log(error);
  }
}

function sleep(ms) {
  console.log(" running maximum number of browsers");
  return new Promise((resolve) => setTimeout(resolve, ms));
}

app.listen(PORT);
console.log(`Running on port: ${PORT}`);
