const runLikesTask = require("../../cluster/tasks/likes/run-task");

class PuppeteerManager {
  constructor(args) {
    this.url = args.url;
    this.existingCommands = args.commands;
    this.userLikes = {};
  }

  async runPuppeteer() {
    console.log("runPuppeteer");
    const puppeteer = require("puppeteer");
    // let commands = [];
    // if (this.nrOfPages > 1) {
    //   for (let i = 0; i < this.nrOfPages; i++) {
    //     if (i < this.nrOfPages - 1) {
    //       commands.push(...this.existingCommands);
    //     } else {
    //       commands.push(this.existingCommands[0]);
    //     }
    //   }
    // } else {
    //   commands = this.existingCommands;
    // }
    const commands = this.existingCommands || [];
    console.log("commands length", commands.length);

    const browser = await puppeteer.launch({
      headless: true,
      args: ["--no-sandbox", "--disable-gpu"],
    });

    let page = await browser.newPage();
    await page.setRequestInterception(true);
    page.on("request", (request) => {
      if (["image"].indexOf(request.resourceType()) !== -1) {
        request.abort();
      } else {
        request.continue();
      }
    });

    await page.on("console", (msg) => {
      for (let i = 0; i < msg._args.length; ++i) {
        msg._args[i].jsonValue().then((result) => {
          console.log(result);
        });
      }
    });

    /*
      locatorCss as task-based css selector to confirm page has loaded
    */

    let timeout = 6000;
    let commandIndex = 0;
    while (commandIndex < commands.length) {
      try {
        console.log(`command ${commandIndex + 1}/${commands.length}`);
        let frames = page.frames();
        await frames[0].waitForSelector(commands[commandIndex].locatorCss, {
          timeout: timeout,
        });
        await this.executeCommand(frames[0], commands[commandIndex], page);
        await this.sleep(1000);
      } catch (error) {
        console.log(error);
        break;
      }
      commandIndex++;
    }
    console.log("done");
    await browser.close();
  }

  async executeCommand(frame, command, page) {
    // await console.log(command.type, command.locatorCss);
    switch (command.type) {
      case "click":
        try {
          await frame.$eval(command.locatorCss, (element) => element.click());
          return true;
        } catch (error) {
          console.log("error", error);
          return false;
        }
      case "getUserLikes":
        try {
          let userLikes = await frame
            .evaluate(async (command) => {
              try {
                await page.goto(this.url);
                const likes = await runLikesTask(page, command.user.name);
                console.log(`LIKES COMPLETE ${name}: `, likes);
                return likes;
                // let parsedItems = [];
                // let items = document.querySelectorAll(command.locatorCss);
                // items.forEach((item) => {
                //   let link =
                //     "http://books.toscrape.com/catalogue/" +
                //     item
                //       .querySelector("div.image_container a")
                //       .getAttribute("href")
                //       .replace("catalogue/", "");
                //   let starRating = item
                //     .querySelector("p.star-rating")
                //     .getAttribute("class")
                //     .replace("star-rating ", "")
                //     .toLowerCase()
                //     .trim();
                //   let title = item.querySelector("h3 a").getAttribute("title");
                //   let price = item
                //     .querySelector("p.price_color")
                //     .innerText.replace("£", "")
                //     .trim();
                //   let book = {
                //     title: title,
                //     price: parseInt(price),
                //     rating: wordToNumber(starRating),
                //     url: link,
                //   };
                //   parsedItems.push(book);
                // });
                // return parsedItems;
              } catch (error) {
                console.log(error);
              }
            }, command)
            .then((result) => {
              this.userLikes.push.apply(this.userLikes, result);
              console.log("userLikes length ", this.userLikes.length);
            });
          return true;
        } catch (error) {
          console.log("error", error);
          return false;
        }
    }
  }

  sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  async getUserLikes() {
    console.log("getUserLikes");
    await this.runPuppeteer();
    return this.userLikes;
  }
}

module.exports = { PuppeteerManager };
