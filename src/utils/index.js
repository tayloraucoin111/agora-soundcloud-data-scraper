const delay = (time) => {
  return new Promise(function (resolve) {
    setTimeout(resolve, time);
  });
};

const getTextFromElement = async (page, element, waitForSelector) => {
  if (waitForSelector) await page.waitForSelector(waitForSelector);
  return await page.evaluate((el) => el.textContent, element);
};

const getTextFromSelector = async (page, selector) => {
  await page.waitForSelector(selector);
  return await getTextFromElement(page, await page.$(selector));
};

const objectToArray = (object) => Object.keys(object).map((key) => object[key]);

const reduceCharacters = (string) => {
  return string.toLowerCase().replace(/[^0-9a-z-]/g, "");
};

const selectorExists = async (page, selector) => {
  return await page.$$(selector);
};

function sorter(items, condition, field) {
  let sorted;

  switch (condition) {
    case "number_highest":
      sorted = sortByNumberHighest(items, field);
      break;
    case "number_lowest":
      sorted = sortByNumberLowest(items, field);
      break;
    default:
      if (items) sorted = items.sort(sortArray);
      break;
  }

  function sortArray(a, b) {
    if (a < b) return -1;
    if (a > b) return 1;
    return 0;
  }

  function sortByNumberHighest(list, key) {
    return list.sort((a, b) => b[key] - a[key]);
  }

  function sortByNumberLowest(list, key) {
    return list.sort((a, b) => a[key] - b[key]);
  }

  return sorted;
}

const typeInInput = async (page, selector, text) => {
  console.log("focus", selector);
  await page.waitForSelector(selector);
  await page.type(selector, text);
};

module.exports = {
  delay,
  getTextFromElement,
  getTextFromSelector,
  objectToArray,
  reduceCharacters,
  selectorExists,
  sorter,
  typeInInput,
};
