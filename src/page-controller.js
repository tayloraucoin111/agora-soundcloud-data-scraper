const artistLikes = require("./jobs/artist-likes/artist-likes");
const feed = require("./jobs/feed/feed");

async function scrapeAll(browserInstance) {
  let browser;
  try {
    browser = await browserInstance;

    const runJob = "artistLikes";
    // const runJob = "feed";

    if (runJob === "artistLikes") artistLikes(browser);
    if (runJob === "feed") feed(browser);
  } catch (err) {
    console.log("Could not resolve the browser instance => ", err);
  }
}

module.exports = (browserInstance) => scrapeAll(browserInstance);
