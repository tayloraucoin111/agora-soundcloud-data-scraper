const fs = require("fs");

const pageScraper = require("./page-scraper");

const feed = async (browser) => {
  const timeLimit = "5 months ago";
  //   const timeLimit = "1 day ago";

  const getData = async () => {
    let page = await browser.newPage();

    const usePageScraper = { ...pageScraper };

    return await usePageScraper.scraper(page, timeLimit);
  };

  const data = await getData();
  console.log("data", data);

  const compileData = () => {
    const matrix = {};
    data.forEach((item) => {
      if (matrix[item.posterName]) {
        matrix[item.posterName].data.push(item);
      } else {
        matrix[item.posterName] = {
          data: [item],
        };
      }
    });
    Object.keys(matrix).forEach((key) => {
      matrix[key].totalCount = matrix[key].data.length;
    });
    return matrix;
  };

  fs.writeFile(
    "data.json",
    JSON.stringify(compileData()),
    "utf8",
    function (err) {
      if (err) {
        return console.log(err);
      }
      console.log(
        "The data has been scraped and saved successfully! View it at './data.json'"
      );
    }
  );
};

module.exports = feed;
