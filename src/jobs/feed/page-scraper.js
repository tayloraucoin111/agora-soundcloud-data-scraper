const { delay, getTextFromElement } = require("../../utils");

const baseUrl = "https://soundcloud.com/stream";

/* SOUNDCLOUD WON'T LET THIS WORK - IT THROTTLES */

const scraperObject = {
  url: baseUrl,
  async scraper(page, timeLimit) {
    // const timeStart = new Date().getTime();
    this.url = baseUrl;

    console.log(`Navigating to ${this.url}...`);
    // Navigate to the selected page
    await page.goto(this.url);

    // Wait for the required DOM to be rendered
    await page.waitForSelector("#app");

    // await page.click(".frontHero__signin .login");

    await delay(14400);
    // colorwheeloflife@gmail.comAgora10108228

    const delayRate = 1500;
    let viewportIncr = 0;
    async function scrollToBottom() {
      // Get the height of the rendered page
      const bodyHandle = await page.$("body");
      const { height } = await bodyHandle.boundingBox();
      await bodyHandle.dispose();

      // Scroll one viewport at a time, pausing to let content load
      const viewportHeight = page.viewport().height;

      /* Find limit reached to stop scroll */
      const findLimitReached = async () => {
        const timeElements = await page.$$(".relativeTime");
        if (timeElements.length > 20) {
          const useTimeElements = timeElements.slice(timeElements.length - 20);
          const getTexts = async () => {
            return Promise.all(
              useTimeElements.map(async (el) => {
                return await getTextFromElement(page, el);
              })
            );
          };
          const timeStamps = await getTexts();
          if (
            timeStamps.find((timeStamp) => timeStamp.indexOf(timeLimit) > -1)
          ) {
            return true;
          }
        }
        return false;
      };
      let limitReached = await findLimitReached();

      if (!limitReached) {
        await page.evaluate((_viewportHeight) => {
          window.scrollBy(0, _viewportHeight);
        }, viewportHeight);
        await delay(delayRate);
        viewportIncr = viewportIncr + viewportHeight;
        await scrollToBottom();
      } else {
        return;
      }
    }
    await scrollToBottom();

    let data = await page.evaluate(() => {
      window.scrollTo(0, 0);
      let tracks = [];
      let trackElements = document.querySelectorAll(".streamContext");

      trackElements.forEach((trackElement) => {
        let trackJson = {};
        try {
          trackJson.posterName = trackElement.querySelector(
            ".soundContext__usernameLink"
          ).innerText;
          trackJson.posterUrl = trackElement.querySelector(
            ".soundContext__usernameLink"
          ).href;

          trackJson.artistName = trackElement.querySelector(
            ".sound__content .soundTitle__username"
          ).innerText;
          trackJson.artistUrl = trackElement.querySelector(
            ".sound__content .soundTitle__username"
          ).href;
          trackJson.trackLikes = trackElement.querySelector(
            ".sc-button-like"
          ).innerText;
          trackJson.trackReposts = trackElement.querySelector(
            ".sc-button-repost"
          ).innerText;
          trackJson.title = trackElement.querySelector(
            ".soundTitle__title"
          ).innerText;
          trackJson.url = trackElement.querySelector(".soundTitle__title").href;
        } catch (e) {
          console.log(e);
        }
        tracks.push(trackJson);
      });
      return tracks;
    });
    await delay(100);
    return data;
  },
};

module.exports = scraperObject;
