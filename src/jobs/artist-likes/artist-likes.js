const bluebird = require("bluebird");
const fs = require("fs");

const { objectToArray, sorter } = require("../../utils");
const FRACTAL_JSON = require("../../constants/fractal.json");
const pageScraper = require("./page-scraper");

const artistLikes = async (browser) => {
  let scrapedData = {};

  // [...new Set(shopsInOrder)],

  function sortByAlphabetical(a, b) {
    if (a < b) return -1;
    if (a > b) return 1;
    return 0;
  }

  const artists = [
    ...new Set(
      FRACTAL_JSON.map((item) => {
        return item.name;
      })
    ),
  ].sort(sortByAlphabetical);

  console.log("artists", artists);
  console.log("artists.length", artists.length);

  // const getData = async () => {
  //   let page = await browser.newPage();
  //   return artists.reduce(async (previousPromise, artistName) => {
  //     await previousPromise;
  //     const usePageScraper = { ...pageScraper };

  //     const scrap = async () => {
  //       console.log("STARTING ARTIST", artistName);
  //       const values = await usePageScraper.scraper(page, artistName);
  //       console.log(`VALUES FOR ${artistName}`, values);
  //       scrapedData[artistName] = { ...values };
  //       return values;
  //     };

  //     return scrap();
  //   }, Promise.resolve());
  // };
  const getData = async () => {
    return await bluebird.map(
      artists,
      async (artistName) => {
        let page = await browser.newPage();
        const usePageScraper = { ...pageScraper };

        console.log("STARTING ARTIST", artistName);
        const values = await usePageScraper.scraper(page, artistName);
        console.log(`VALUES FOR ${artistName}`, values);
        scrapedData[artistName] = { ...values };
        page.close();
        return values;
      },
      { concurrency: 15 }
    );
  };
  await getData();
  console.log("getData completed");

  const clearOneOffs = (matrix) => {
    const cleared = {};
    Object.keys(matrix).forEach((key) => {
      if (matrix[key].quantity > 1) {
        cleared[key] = matrix[key];
      }
    });
    return cleared;
  };

  const compileData = () => {
    const inputMatrix = {};

    const createOrPush = (item, artistName) => {
      if (inputMatrix[item.url]) {
        inputMatrix[item.url].quantity = inputMatrix[item.url].quantity + 1;
        inputMatrix[item.url].likedBy.push(artistName);
      } else {
        inputMatrix[item.url] = {
          data: item,
          likedBy: [artistName],
          quantity: 1,
        };
      }
    };

    Object.keys(scrapedData).forEach((key) => {
      const artistData = scrapedData[key];
      if (typeof artistData.data == "string") return;
      artistData.data.forEach((item) => {
        createOrPush(item, key);
      });
    });

    return clearOneOffs(inputMatrix);
  };

  const compiledData = compileData();

  console.log("compiledData", compiledData);

  const sorted = sorter(
    objectToArray(compiledData),
    "number_highest",
    "quantity"
  );
  console.log("sorted", sorted);

  fs.writeFile(
    "data.json",
    JSON.stringify({
      artistData: scrapedData,
      trackData: compiledData,
    }),
    "utf8",
    function (err) {
      if (err) {
        return console.log(err);
      }
      console.log(
        "The data has been scraped and saved successfully! View it at './data.json'"
      );
    }
  );
};

module.exports = artistLikes;
