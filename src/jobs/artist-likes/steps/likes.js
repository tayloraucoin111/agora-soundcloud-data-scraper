const { delay } = require("../../../utils");

const captureLikes = async (page) => {
  await page.waitForSelector(".soundTitle__title");

  console.log("starting likes scrap...");

  const delayRate = 800;

  let viewportIncr = 0;

  async function scrollToBottom() {
    // Get the height of the rendered page
    const bodyHandle = await page.$("body");
    const { height } = await bodyHandle.boundingBox();
    await bodyHandle.dispose();

    // Scroll one viewport at a time, pausing to let content load
    const viewportHeight = page.viewport().height;

    while (viewportIncr + viewportHeight < height) {
      await page.evaluate((_viewportHeight) => {
        window.scrollBy(0, _viewportHeight);
      }, viewportHeight);
      await delay(delayRate);
      viewportIncr = viewportIncr + viewportHeight;
      await scrollToBottom();
    }
    if (viewportIncr + viewportHeight === height) {
      return;
    }
  }
  await scrollToBottom();

  let data = await page.evaluate(() => {
    window.scrollTo(0, 0);
    let products = [];
    let productElements = document.querySelectorAll(".sound__content");

    productElements.forEach((productElement) => {
      let productJson = {};
      try {
        productJson.artistName = productElement.querySelector(
          ".sound__content .soundTitle__username"
        ).innerText;
        productJson.artistUrl = productElement.querySelector(
          ".sound__content .soundTitle__username"
        ).href;
        productJson.trackLikes = productElement.querySelector(
          ".sc-button-like"
        ).innerText;
        productJson.trackReposts = productElement.querySelector(
          ".sc-button-repost"
        ).innerText;
        productJson.title = productElement.querySelector(
          ".soundTitle__title"
        ).innerText;
        productJson.url = productElement.querySelector(
          ".soundTitle__title"
        ).href;
      } catch (e) {
        console.log(e);
      }
      products.push(productJson);
    });
    return products;
  });
  await delay(100);
  return data;
};

module.exports = {
  captureLikes,
};
