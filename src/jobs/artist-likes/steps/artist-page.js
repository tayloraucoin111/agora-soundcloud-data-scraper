const { getTextFromSelector, selectorExists } = require("../../../utils");

const getLikesCount = async (page) => {
  page.evaluate((_) => {
    window.scrollBy(500, 500);
  });

  if (
    selectorExists(
      page,
      ".likesModule .sidebarHeader__title .sidebarHeader__actualTitle"
    )
  ) {
    const likesTextValue = await getTextFromSelector(
      page,
      ".likesModule .sidebarHeader__title .sidebarHeader__actualTitle"
    );
    return likesTextValue ? likesTextValue.split(" likes")[0] : 0;
  }

  return 0;
};

module.exports = {
  getLikesCount,
};
