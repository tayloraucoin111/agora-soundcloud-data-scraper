const { captureLikes } = require("./steps/likes");
const { getLikesCount } = require("./steps/artist-page");
const { searchArtist } = require("./steps/search-artist");

// async function pressEnter(page) {
//     return await page.type(String.fromCharCode(13));
// }

const baseUrl = "https://soundcloud.com/search?q=";

function timeDiff(tstart, tend) {
  var diff = Math.floor((tend - tstart) / 1000),
    units = [
      { d: 60, l: "seconds" },
      { d: 60, l: "minutes" },
      { d: 24, l: "hours" },
    ];

  var s = "";
  for (var i = 0; i < units.length; ++i) {
    s = (diff % units[i].d) + " " + units[i].l + " " + s;
    diff = Math.floor(diff / units[i].d);
  }
  return s;
}

const scraperObject = {
  url: baseUrl,
  async scraper(page, name) {
    const timeStart = new Date().getTime();
    this.url = `${baseUrl}/${name}`;

    console.log(`Navigating to ${this.url}...`);
    // Navigate to the selected page
    await page.goto(this.url);

    // Wait for the required DOM to be rendered
    await page.waitForSelector("#app");

    /* Find artist from list and click to enter page */
    const artistFound = await searchArtist(page, name);
    console.log("artistFound", artistFound);

    if (!artistFound) {
      return {
        data: `no artist found for ${name}`,
      };
    }

    const likesTotalCount = await getLikesCount(page);
    console.log("likesTotalCount", likesTotalCount);
    if (!likesTotalCount) {
      return {
        data: `no likes found for ${name}`,
      };
    }

    const currentUrl = await page.url();
    await page.goto(`${currentUrl}/likes`);
    // await page.click(".likesModule .sidebarHeader__actualTitle");

    const likesData = likesTotalCount && (await captureLikes(page));

    const timeEnd = new Date().getTime();

    const timeTaken = timeDiff(timeStart, timeEnd);

    console.log(
      `TIME TO COMPLETE ${likesTotalCount} for ${name} was ${timeTaken}`
    );
    console.log(
      `RATIO OF PROCESSING TIME PER LIKE WAS ${
        (timeEnd - timeStart) / 1000 / likesTotalCount
      }`
    );

    return {
      data: likesData || [],
      likesTotalCount: likesTotalCount
        ? parseInt(likesTotalCount)
        : likesData.length,
      matchRate: likesTotalCount && likesTotalCount - likesData.length,
      unixTimeTaken: timeEnd - timeStart,
    };
    // await delay(100000000);
  },
};

module.exports = scraperObject;
