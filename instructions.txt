To build raw

1. Execution script
2. Execute command type
3. identify data list
4. set data store
5. set max-browser instances
6. set activeBrowser to 0
6. map data list
6a. include a while (activeBrowser == maxNumberOfBrowsers) condition
6b. set activeBrowser +1

7. execute commands
7a. launch browser with head; modeling original version
7b. scroll to the bottom of artist likes list 
7c. scrape all track node values

8. set returned command value into data store
9. wait for all commands to have executed
10. assemble data for file write
11. write json file